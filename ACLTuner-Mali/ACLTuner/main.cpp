#include <cxxopts/cxxopts.hpp>
#include <exec.h>
#include <arm_compute/runtime/Scheduler.h>
#include <asm/hwcap.h> /* Get HWCAP bits from asm/hwcap.h */
#include <sys/auxv.h>
#include <thread>
#include <fstream>
#include <helper.h>

int main(int argc, const char *argv[])
{
#if DEBUG_LOG == 1
    printf("GCC VERSION : %d.%d.%d\n", __GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__);
    set_thread_affinity(0);
#endif

    cxxopts::Options options("test", "A brief description");
    options.add_options()
            ("o,model", "model [./vgg16.tflite]", cxxopts::value<std::string>())
            ("m,mode", "tune [loggingable default(-1), default(0), log(1), optimize(2)]", cxxopts::value<int>())
            ("c,conv", "convolution mode [0-16, 0]", cxxopts::value<int>()->default_value("-1"))
            ("l,log", "log file name", cxxopts::value<std::string>()->default_value("./result.csv"))
            ("a,log_log", "log_log", cxxopts::value<std::string>()->default_value(""))
            ("e,evals", "eval", cxxopts::value<int>()->default_value("10"))
        ;

    auto result = options.parse(argc, argv);
    int mode = result["mode"].as<int>();

    if (mode == 0 || mode == -1) { 
        exec::exec_default(result["model"].as<std::string>(), 1, 
                           result["evals"].as<int>(), 
                           result["log"].as<std::string>(), 
                           mode == -1);
    }else if (mode == 1) { 
        exec::exec_log(result["model"].as<std::string>(), 
                       result["log"].as<std::string>(), 1, 
                       result["evals"].as<int>(), 
                       result["log_log"].as<std::string>());
    }else { 
        exec::optimize(result["model"].as<std::string>(), 
                result["log"].as<std::string>(), 1, 
                result["evals"].as<int>(), 
                result["conv"].as<int>());
    }

    return 0;
}
