#include <tuner.h>
#include <helper.h>
#include <nlohmann/json.hpp>
#include <fstream>

#define IP "192.168.0.7"
#define PORT 9666

namespace exec { 

bool optimize(std::string model, std::string save_opt_file, int repeat, int evals, int conv_mode) { 
    tuner_manager<float> tun;
    std::ofstream log(save_opt_file);

    tun.set_tuner(tuner_option { 
        conv_mode,
        true,
        tuner_type::opt_tpe,
    }, "", 0, evals, repeat);
    
    int max_conv_size = tun.init(model, "", true);
    int conv_count = opencl_convolution_count;
    std::cout << "conv count : " << conv_count << "\n";

    std::vector<extract_inference> inference;

    for (int j = 0; j < opencl_convolution_name.size(); j++) { 
        log << opencl_convolution_name[j] << ",";
    }
    
    log << "total";
    log << "\n";

    for (int i = 0; i < evals; i++) { 
        opencl_convolution.clear();
        auto infer = tun.inference();
        for (int j = 0; j < opencl_convolution.size(); j++) { 
            log << opencl_convolution[j].score << ",";
        }

        log << infer.inference_time;
        log << "\n";
        
        std::cout << i + 1 << " 반복함 (" << infer.inference_time / 1000 << " us)" << std::endl;
        // if (REMOVE_CACHE) {
        //     remove_cache();
        // }
    }

    std::cout << " 최종적인 결과를 반환함. " << std::endl;
    return true;
}

void exec_log(std::string model, std::string opt_file, int repeat, int evals, std::string log_file) { 
    tuner_manager<unsigned char> tun;
    std::ofstream log(log_file);
    
    tun.set_tuner(tuner_option { 
        0,
        true,
        tuner_type::opt_tpe,
    }, "", 0, evals, repeat);
    
    int max_conv_size = tun.init(model, opt_file, true);
    
    for (int j = 0; j < opencl_convolution_name.size(); j++) { 
        log << opencl_convolution_name[j] << ",";
    }
    
    log << "total";
    log << "\n";


    std::vector<extract_inference> inference;
    for (int i = 0; i < evals; i++) { 
        opencl_convolution.clear();
        auto infer = tun.inference();
        for (int j = 0; j < opencl_convolution.size(); j++) { 
            log << opencl_convolution[j].score << ",";
        }
        // log_extract_inference_console(infer);
        // if (REMOVE_CACHE) {
        //     remove_cache();
        // }
        log << infer.inference_time;
        log << "\n";

        std::cout << i + 1 << " 반복함 (" << infer.inference_time / 1000 << " us)" << std::endl;
    }

    std::cout << " 최종적인 결과를 반환함. " << std::endl;
}


void exec_default(std::string model, int repeat, int evals, std::string logfile, bool loggingable) { 
    tuner_manager<float> tun;
    std::ofstream log(logfile);

    tun.set_tuner(tuner_option { 
        -1,
        true,
        tuner_type::opt_tpe,
    }, "", 0, evals, repeat);
    
    int max_conv_size = tun.init(model, "", loggingable);
    int conv_count = opencl_convolution_count;
    std::cout << "conv count : " << conv_count << "\n";

    std::vector<extract_inference> inference;

    for (int j = 0; j < opencl_convolution_name.size(); j++) { 
        log << opencl_convolution_name[j] << ",";
    }
    log << "total";
    log << "\n";

    for (int i = 0; i < evals; i++) { 
        opencl_convolution.clear();
        auto infer = tun.inference();
        for (int j = 0; j < opencl_convolution.size(); j++) { 
            log << opencl_convolution[j].score << ",";
        }
        log << infer.inference_time;
        log << "\n";
        
        std::cout << i + 1 << " 반복함 (" << infer.inference_time / 1000 << " us)" << std::endl;
        // if (REMOVE_CACHE) {
        //     remove_cache();
        // }
    }

    std::cout << " 최종적인 결과를 반환함. " << std::endl;
}
}
