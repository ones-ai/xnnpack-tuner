#pragma once

#include <iostream>
#include <chrono>
#include <string>
#include <vector>
#include <tuple>
#include <stdio.h>



#include <luna/neon.h>
#include <search/optimizer.h>
#include <armnn/ArmNN.hpp>

#include <fstream>
#include <nlohmann/json.hpp>

using namespace std;

struct tuner_option { 
    int convolution_optimizer = 0;
    bool workload_optimizer = false;
    // bool print_core_time = false;
    // bool print_workload_time = false;
    // bool print_layer_time = false;
    // bool print_inference_time = false;
    tuner_type type = tuner_type::none;
};

struct extract_kernel_work
{
    int core_id;
    long long compute_time;
};

struct extract_kernel { 
    int guid;
    long long kernel_time;
    string kernel_name;
    string layer_name;
    int convolution_id = -1;
    tuner_data tune_data;
    vector<extract_kernel_work> core_time;
};

struct extract_inference { 
    long long inference_time;
    long long black_box_time;
    vector<extract_kernel> kernels;
};

template<typename T>
class tuner_manager { 
private:
    tuner_option option;
    neon_engine<T> engine;
    int once_tuner_info = 0;
    extract_inference result_tmp;
    extract_kernel kernel_tmp;
    int layer_guid = -1;
    int conv_id = 0;
    optimizer opt;
    int repeat = 1;
    std::vector<long long> measure_time;

    bool hing = false;

private:
    bool default_runner = false;
    bool fix_mode = false;
    bool is_first_exec = true;
    string cur_hash_name;
    tuner_data cur_tuner_data;

    int tune_id = 0;
    std::vector<int> mode;

public:
    void set_default(bool run) { 
        fix_mode = true;
        this->default_runner = run;
    }

    bool set_tuner(tuner_option option, std::string ip, int port, int evals, int repeat) { 
        this->option = option;
        this->repeat = repeat; 
#if DEBUG_LOG == 1
        std::cout << "test3" << std::endl;
#endif
        opt.set_type(option.type, ip, port, repeat, evals);
#if DEBUG_LOG == 1
        std::cout << "test4" << std::endl;
#endif
        return true;
    }

    int init(std::string model, std::string arch = "", bool isOptMode = true) { 
        if (arch != ""){ 
            std::ifstream opt(arch);
            while (!opt.eof()) {
                int data;
                opt >> data;
                mode.push_back(data);
            }
        }
        std::cout << "----\n";
        for (int i = 0; i < mode.size(); i++) { 
            std::cout << mode[i] << " ";
        }
        std::cout << "\n----\n";
        std::cout << this->option.convolution_optimizer << "\n";
        std::cout << "----\n";

        if (option.convolution_optimizer != -1) { 
            cl_callback = [&](bool gemm, bool direct, bool winograd, bool fft) {
                auto id = this->mode.size() == 0 ? this->option.convolution_optimizer : this->mode[this->conv_id];
                this->conv_id += 1;
                int method = (id / 10);
                int kernel_type = (id % 10);
                arm_compute::ConvolutionMethod m;
                arm_compute::CLGEMMKernelType t;
                switch (method)
                {
                case 0:
                    m = arm_compute::ConvolutionMethod::GEMM;
                    break;
                case 1:
                    m = arm_compute::ConvolutionMethod::WINOGRAD;
                    break;
                case 2:
                    m = arm_compute::ConvolutionMethod::DIRECT;
                    break;
                default:
                    throw "metohd error!";
                }
                switch (kernel_type)
                {
                case 0:
                    t = arm_compute::CLGEMMKernelType::NATIVE;
                    break;
                case 1:
                    t = arm_compute::CLGEMMKernelType::RESHAPED;
                    break;
                case 2:
                    t = arm_compute::CLGEMMKernelType::RESHAPED_ONLY_RHS;
                    break;
                default:
                    throw "kernel tpye error!";
                }
                // std::cout << gemm << " " << direct << " " << winograd << " " << fft << "\n";
                return std::make_tuple(m, t);
            };
        }else { 
            cl_callback = nullptr;
        }

        engine = neon_engine<T>();
        engine.init(model, isOptMode);

        this->is_first_exec = true; 
        engine.inference();
        this->is_first_exec = false;

        return 1000;
    }

    void export_best(std::string file) { 
        // std::ofstream of(file);
        // of << opt.export_best();
        // of.close();
    }
    void execute_log(std::string file) { 
        // std::ifstream idata(file);
        // opt.import_best(idata);
        // fix_mode = true;
    }
    void excute_best() { 
        // opt.run_best();
        // fix_mode = true;
    }

    extract_inference inference() {
        tune_id = 0;
        conv_id = 0;

        this->result_tmp.kernels.clear();
        auto start = high_resolution_clock::now();
        engine.inference();
        auto end = high_resolution_clock::now();
        // if (!fix_mode) { 
        //     this->opt.next_param();
        // }
        this->result_tmp.black_box_time = (high_resolution_clock::now() - end).count();
        this->result_tmp.inference_time = (end - start).count();
        return this->result_tmp;
    }

    void deinit() {  
        engine.deinit();
    }
};
