#pragma once

#include <string>

namespace exec { 
    bool optimize(std::string model, std::string save_opt_file, int repeat, int evals, int conv_mode);
    void exec_log(std::string model, std::string opt_file, int repeat, int evals, std::string log_file) ;
    void exec_default(std::string mode, int repeat, int evals, std::string logfile, bool loggingable = false);
}
