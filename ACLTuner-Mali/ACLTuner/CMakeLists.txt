cmake_minimum_required(VERSION 3.4.1)
SET(PROJ_NAME chacha)
project(${PROJ_NAME})

add_compile_definitions(REMOVE_CACHE=1
						CORE_COUNT=6
						DEBUG_LOG=1
						DEFAULT_CORE=4,5
						WARM_UP_CORE=0,1,2,3,4,5
                        )


set(CMAKE_C_STANDARD                99)
set(CMAKE_CXX_STANDARD              14)
set(CMAKE_C_STANDARD_REQUIRED       ON)
set(CMAKE_CXX_STANDARD_REQUIRED     ON)
set(CMAKE_C_EXTENSIONS              OFF)
set(CMAKE_CXX_EXTENSIONS            OFF)
set(CMAKE_C_FLAGS_DEBUG         "-DDEBUG -O0 -g -fPIC -lpthread -ldl")
set(CMAKE_C_FLAGS_RELEASE       "-DNDEBUG -O3 -fPIC -lpthread -ldl")
set(CMAKE_CXX_FLAGS_DEBUG       "-DDEBUG -O0 -g -fPIC -lpthread -ldl")
set(CMAKE_CXX_FLAGS_RELEASE     "-DNDEBUG -O3 -fPIC -lpthread -ldl")
set(BUILD_SHARED_LIBS OFF)
SET(STATIC_LINK ON)

# set(CMAKE_EXE_LINKER_FLAGS "-static")

set(JSON_BuildTests OFF CACHE INTERNAL "")

add_subdirectory(${CMAKE_SOURCE_DIR}/module/json)

include_directories(${CMAKE_SOURCE_DIR}/../armnn/include)
include_directories(${CMAKE_SOURCE_DIR}/../armnn/samples/common/include/Utils)
include_directories(${CMAKE_SOURCE_DIR}/../armnn/samples/common/include/ArmnnUtils)
include_directories(${CMAKE_SOURCE_DIR}/../armnn/delegate/include)
include_directories(${CMAKE_SOURCE_DIR}/../armnn/delegate/src)
include_directories(${CMAKE_SOURCE_DIR}/../armnn/profiling)

include_directories(${CMAKE_SOURCE_DIR}/../tensorflow)
include_directories(${CMAKE_SOURCE_DIR}/../flatbuffers/include)

include_directories(${CMAKE_SOURCE_DIR}/../ComputeLibrary/include)
include_directories(${CMAKE_SOURCE_DIR}/../ComputeLibrary)

# library headers
message(${CMAKE_SOURCE_DIR})

include_directories(${CMAKE_SOURCE_DIR}/include)
include_directories(${CMAKE_SOURCE_DIR}/module)

message("Sources : " ${SOURCE_FILES})

link_directories(${CMAKE_SOURCE_DIR}/lib)

add_executable(${PROJ_NAME}
	main.cpp
	src/stb_image.cpp
	src/helper.cpp
	src/exec.cpp
	
	src/strategy_factory.cpp
	src/tune_engine.cpp
	
	src/optimizer.cpp
	src/optimizer/optuna.cpp
	src/optimizer/random.cpp
	
	module/HappyHTTP/happyhttp.cpp
)


target_link_libraries(${PROJ_NAME} 
	pthread
	dl
	
	arm_compute_core
	arm_compute_graph
	arm_compute

	armnn
	armnnTfLiteParser
	
	protobuf-lite
	protobuf
	protoc
	flatbuffers
	armnnDelegate
	
	tensorflowlite
)
 
# target_link_libraries(${PROJ_NAME} -static-libgcc -static-libstdc++ )


target_link_libraries(${PROJ_NAME} nlohmann_json::nlohmann_json)

