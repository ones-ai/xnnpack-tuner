# python3 -m pip install xgboost optuna scikit-learn numpy 

import numpy as np
import optuna as opt

import numpy as np
from flask import request, Flask
import threading
import argparse
import time

import logging 
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

opt.logging.disable_default_handler()
# layer_idx = 0
server_port = None
def str_to_int(data):
    try:
        return int(data)
    except:
        return None    


lock = threading.Lock() # threading에서 Lock 함수 가져오기
for_search_data = False # threading에서 Lock 함수 가져오기
# 입력 결괏값을 저장한 Trials 객체값 생성.

search_data = None
search_data_score = None
best_idx = None
thread_create_fin = False
seach_space_size = 0
opt_name = ""
def objective(trial):
    global search_data
    global search_data_score
    global thread_create_fin
    global for_search_data
    global seach_space_size

    x = trial.suggest_int('x', 0, seach_space_size - 1)
    #print(x)
    search_data = x
   # print(search_data)
    
    thread_create_fin = True
    for_search_data = True
    while search_data_score == None:
        time.sleep(0.1)

    result = search_data_score
    # print(search_data_score)
    search_data_score = None
    # 요청해서 응답받기
    # print(retval)
    return result

def thread_data(evalsss: int, sspace: int):
    global best_idx
    global thread_create_fin
    global seach_space_size
    seach_space_size = sspace
    # global layer_idx
    global opt_name
    if opt_name == "random":
        study = opt.create_study(direction="minimize", sampler=opt.samplers.RandomSampler())
    elif opt_name == "grid":
        study = opt.create_study(direction="minimize", sampler=opt.samplers.GridSampler())
    elif opt_name == "tpe":
        study = opt.create_study(direction="minimize", sampler=opt.samplers.TPESampler())
    elif opt_name == "cma":
        study = opt.create_study(direction="minimize", sampler=opt.samplers.CmaEsSampler())
    elif opt_name == "pf":
        study = opt.create_study(direction="minimize", sampler=opt.samplers.PartialFixedSampler())
    elif opt_name == "nsga2":
        study = opt.create_study(direction="minimize", sampler=opt.samplers.NSGAIISampler())
    elif opt_name == "qmc":
        study = opt.create_study(direction="minimize", sampler=opt.samplers.QMCSampler())
    else:
        raise Exception('what the?!')
    
    
    study.optimize(objective, n_trials=evalsss)
  #  print(f"Sampler is {study.sampler.__class__.__name__}")

   # print("Number of finished trials: ", len(study.trials))
   # print("Best trial:")
    trial = study.best_trial

   # print("  Value: {}".format(trial.value))
   # print("  Params: ")
    for key, value in trial.params.items():
        print("    {}: {}".format(key, value))

    best_idx = None

    # best_idx = fmin(fn=objective_func, space=search_space, algo=rand.suggest, max_evals=evalsss
    #            , trials=trial_val, rstate=np.random.default_rng(seed=0))
    print(best_idx)
    thread_create_fin = False

app = Flask(__name__)

@app.route('/next')
def flask_next():
    global search_data
    global search_data_score
    lock.acquire() 
    
    if search_data != None:
        # time.sleep(0.1)
        print("next error!")
        return "error"
    
    score = request.args.get('score', None)
    value = str_to_int(score)
    if value == None: 
        return "error"
    
    search_data_score = value    
    lock.release() 
    return "success"

@app.route('/get')
def flask_get():
    global search_data
    global for_search_data
    lock.acquire()
    while not for_search_data:
        time.sleep(0.1)
    for_search_data = False

    if search_data == None:
        print("get error!")
        return "error"
   # print("get : ", search_data)
    result = str(int(search_data))
    search_data = None
# request.args.get('evlas', None)
    lock.release() 

    return result
    
# 몇번을 실행 할것인지, 경우의 수가 몇가지인지 반환함.
# 갯수 요청 -> 초기 실행 위치 전달
@app.route('/setup')
def flask_setup():
    # global layer_idx
    global thread_create_fin
    lock.acquire()  
    # layer_idx += 1
    while thread_create_fin:
        pass
    
    eval = str_to_int(request.args.get('evals', None))
    search_size = str_to_int(request.args.get('total', None))
    
    if eval == None or search_size == None:
        return "error"
    
    t = threading.Thread(target=thread_data, args=(eval, search_size))
    t.start()
    
    while not thread_create_fin:
        time.sleep(0.01)
    
    lock.release() 
    return "success"

if __name__ == "__main__":
    parser = argparse.ArgumentParser('tflite to rknn')
    parser.add_argument('-p', dest='port', type=int, required=False, help='텐서플로우 파일 이름', default=9000)
    parser.add_argument('-o', dest='opt', type=str, required=False, help='텐서플로우 파일 이름', default='random')
    args = parser.parse_args()
    opt_name = args.opt
    server_port = args.port

    app.run(host='0.0.0.0', port=args.port)
    