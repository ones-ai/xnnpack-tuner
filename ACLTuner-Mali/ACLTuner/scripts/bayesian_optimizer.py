from hyperopt import fmin, tpe, space_eval, Trials, rand, hp
import numpy as np
from flask import request, Flask
import threading
import argparse
import time
import pickle

layer_idx = 0
server_port = None
def str_to_int(data):
    try:
        return int(data)
    except:
        return None        

lock = threading.Lock() # threading에서 Lock 함수 가져오기
for_search_data = False # threading에서 Lock 함수 가져오기
# 입력 결괏값을 저장한 Trials 객체값 생성.

search_data = None
search_data_score = None
best_idx = None
thread_create_fin = False

def objective_func(search_space):
    global search_data
    global search_data_score
    global thread_create_fin
    global for_search_data

    x = search_space['x']
    search_data = x
    # print(search_data)
    
    thread_create_fin = True
    for_search_data = True
    while search_data_score == None:
        time.sleep(0.1)

    result = search_data_score
    # print(search_data_score)
    search_data_score = None
    # retval = x**3 #
    # 요청해서 응답받기
    # print(retval)
    return result

global_search_space = None
def thread_data(evalsss: int, sspace: int):
    global best_idx
    global thread_create_fin
    global layer_idx
    global global_search_space
    global server_port

    trial_val = Trials()
    best_idx = None
    global_search_space = {'x': hp.quniform('x', 0, sspace - 1, 1) }
    len
    best_idx = fmin(fn=objective_func, space=global_search_space, algo=tpe.suggest, max_evals=evalsss
               , trials=trial_val)
    with open(str(server_port) + "_" + str(layer_idx) + ".hyperopt", "wb") as f:
        pickle.dump(trial_val, f)
    # best_idx = fmin(fn=objective_func, space=search_space, algo=rand.suggest, max_evals=evalsss
    #            , trials=trial_val, rstate=np.random.default_rng(seed=0))
    print(best_idx)
    thread_create_fin = False

app = Flask(__name__)

@app.route('/best')
def flask_best():
    lock.acquire() 
    global best_idx
    if best_idx == None:
        return "error"
    result = int(best_idx['x'])
    best_idx = None
    lock.release() 
    return str(result)

@app.route('/next')
def flask_next():
    global search_data
    global search_data_score
    lock.acquire() 
    
    if search_data != None:
        # time.sleep(0.1)
        print("next error!")
        return "error"
    
    score = request.args.get('score', None)
    value = str_to_int(score)
    if value == None: 
        return "error"
    
    search_data_score = value    
    lock.release() 
    return "success"

@app.route('/get')
def flask_get():
    global search_data
    global for_search_data
    lock.acquire()
    while not for_search_data:
        pass
    for_search_data = False

    if search_data == None:
        # time.sleep(0.1)
        print("get error!")
        return "error"
    print("get : ", search_data)
    result = str(int(search_data))
    search_data = None
# request.args.get('evlas', None)
    lock.release() 

    return result
    
# 몇번을 실행 할것인지, 경우의 수가 몇가지인지 반환함.
# 갯수 요청 -> 초기 실행 위치 전달
@app.route('/setup')
def flask_setup():
    global layer_idx
    global thread_create_fin
    lock.acquire()  
    layer_idx += 1
    while thread_create_fin:
        pass
    
    eval = str_to_int(request.args.get('evals', None))
    search_size = str_to_int(request.args.get('total', None))
    
    if eval == None or search_size == None:
        return "error"
    
    t = threading.Thread(target=thread_data, args=(eval, search_size))
    t.start()
    
    while not thread_create_fin:
        time.sleep(0.01)
    
    lock.release() 
    return "success"

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser('tflite to rknn')
    parser.add_argument('-p', dest='port', type=int, required=False, help='텐서플로우 파일 이름', default=9000)
    args = parser.parse_args()
    server_port = args.port

    app.run(host='0.0.0.0', port=args.port)
    