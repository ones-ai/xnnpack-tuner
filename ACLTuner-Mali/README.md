# ACLTuner

Docker is used to build ACL Tuner. To create an executable binary, you only need to enter one line of the command.

If you want to build tensorflow_lite, you need to install bazel and then build v2.5.0 of tensorflow.

[[Click ME!]](https://www.tensorflow.org/lite/guide/build_arm?hl=en).


```sh
# build
# executable binary for Armv8 is export in the Output folder.
# Dependency : make, docker and sudo
$ make build
```

## Dependency

1. ArmNN 22.05.1
2. ArmCL 22.05
3. tensorflow lite 2.5.0
4. flatbuffers 1.12.0
5. nlohmann/json
6. cxxopts
7. HappyHTTP
8. stb_image

