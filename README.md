# ACLTuner

Docker is used to build ACL Tuner. To create an executable binary, you only need to enter one line of the command.

If you want to build tensorflow_lite, you need to install bazel and then build v2.5.0 of tensorflow.

[[Click ME!]](https://www.tensorflow.org/lite/guide/build_arm?hl=en).


```sh
# build
# executable binary for Armv8 is export in the Output folder.
# Dependency : make, docker and sudo
$ make  # default : all
$ make armcpu
$ make mali
$ make xnnpack
```
