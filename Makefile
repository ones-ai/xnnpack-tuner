.PHONY: all xnnpack armcpu mali xnnpack-init armcpu-init mali-init all-init

all: xnnpack armcpu mali

xnnpack-init:
	git submodule update --init --recursive ACLTuner-XNNPack

armcpu-init:
	git submodule update --init --recursive ACLTuner-Neon

mali-init:
	git submodule update --init --recursive ACLTuner-Mali

xnnpack: xnnpack-init
	cd ACLTuner-XNNPack && \
	make build && \
	mv Output ../XNNPack

armcpu: armcpu-init
	cd ACLTuner-Neon # && \
	make build && \
	mv Output ../Neon

mali: mali-init
	cd ACLTuner-Mali && \
	make build && \
	mv Output ../Mali