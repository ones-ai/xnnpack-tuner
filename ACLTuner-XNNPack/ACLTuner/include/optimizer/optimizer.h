#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <HappyHTTP/network.hpp>
#include <sstream>

#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/model.h"

#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

struct op_name { 
    std::string ops;
    std::string kernel;
    int workload;
};

struct init_data { 
    int gemm_config_cnt;
    int gemm_cnt;
    std::vector<op_name> name;
};

struct inference_data { 
    std::vector<long long> inference_time;
    long long xnnpack_inference_total_time;
    long long inference_total_time;
};

class optimizer { 
private:
    int thread_num;
    std::string ip;
    int port;
    std::shared_ptr<tflite::FlatBufferModel> model;
    std::unique_ptr<tflite::Interpreter> interpreter;
    int tuner_size = 0;

public:
    init_data init(int thread_num, std::string file_name, std::vector<int> gemm);
    std::vector<inference_data> inference(std::vector<std::vector<int>> workload, int repeat = 8, bool cache_remove = true);

    void tuner_hard_reset(std::string ip, int port);
    std::vector<int> tuner_create(const std::vector<int>& workload, int sleep_time = 30);

private:
    std::vector<int> tuner_setup(const std::vector<int>& workload);
    void p_tuner_next_workload(const std::vector<long long> score);
    std::vector<int> tuner_get_workload();
public:
    std::vector<int> tuner_next_workload(const std::vector<long long> score);
};