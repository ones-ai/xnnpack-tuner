#include <cstdio>
#include <chrono>
#include <iostream>

#include <optimizer/optimizer.h>

#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/optional_debug_tools.h"
#include "tensorflow/lite/delegates/xnnpack/xnnpack_delegate.h"

#include <xnnpack.h>
#include <tuple>

#include <optimizer/helper.h>

#define TFLITE_MINIMAL_CHECK(x)                              \
  if (!(x)) {                                                \
    fprintf(stderr, "Error at %s:%d\n", __FILE__, __LINE__); \
    exit(1);                                                 \
  }


init_data optimizer::init(int thread_num, std::string file_name, std::vector<int> gemm) { 
    request_config_cnt = 0;

    if (gemm.size() != 0) {
        global_kernel_selection.select_f32_gemm_config = gemm.data();
    }
    else { 
        global_kernel_selection.select_f32_gemm_config = NULL;
    }
    test.thread_num = thread_num;
    model = tflite::FlatBufferModel::BuildFromFile(file_name.c_str());

    global_kernel_selection.select_f32_gemm_config = NULL;
    
    tflite::ops::builtin::BuiltinOpResolver resolver;
    tflite::InterpreterBuilder builder(*model, resolver);
    builder(&interpreter);
    
    interpreter->SetNumThreads(test.thread_num);
    
    TFLITE_MINIMAL_CHECK(interpreter != nullptr);
    // Allocate tensor buffers.
    TFLITE_MINIMAL_CHECK(interpreter->AllocateTensors() == kTfLiteOk);
    // printf("=== Pre-invoke Interpreter State ===\n");
    // tflite::PrintInterpreterState(interpreter.get());

    // Fill input buffers
    // TODO(user): Insert code to fill input tensors.
    // Note: The buffer of the input tensor with index `i` of type T can
    // be accessed with `T* input = interpreter->typed_input_tensor<T>(i);`
    // TFLITE_MINIMAL_CHECK(interpreter->Invoke() == kTfLiteOk);
    // test.worksize = 0;
    // test.is_first_run = true;
    // test.op_name.clear();
    // test.kernel_name.clear();
    // test.workload.clear();

    TFLITE_MINIMAL_CHECK(interpreter->Invoke() == kTfLiteOk);
    for (int i = 0; i < 4; i++){
        auto start = std::chrono::high_resolution_clock::now();
        TFLITE_MINIMAL_CHECK(interpreter->Invoke() == kTfLiteOk);
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << test.inference_total_time / 1000 << "us \n";
    }
    
    int gemm_config_cnt = f32_gemm_config_cnt;
    int request = request_config_cnt;

    std::vector<op_name> name;

    for (int i = 0; i < test.total_worksize; i++) {
        name.push_back(op_name{
            test.op_name[i],
            test.kernel_name[i],
            test.workload[i],
        });
    }

    return {
        gemm_config_cnt, request, name
    };
}

std::vector<inference_data> optimizer::inference(std::vector<std::vector<int>> workload, int repeat, bool cache_remove) { 
    test.set_workload = workload;
    
    // for (int y = 0; y < workload.size(); y++) {
    //     for (int x = 0; x < workload[y].size(); x++) {
    //         printf("%d, ", workload[y][x]);
    //     }
    //     printf("\n");
    // }
    test.inference_time.clear();
    test.op_name.clear();
    test.kernel_name.clear();
    
    std::vector<inference_data> result;
    if (cache_remove) { 
        remove_cache();
    }

    for (int i = 0; i < repeat; i++) { 
        interpreter->Invoke();
        inference_data d;
        d.inference_time = test.inference_time;
        d.inference_total_time = test.inference_total_time;
        d.xnnpack_inference_total_time = test.xnnpack_inference_total_time;

        if (i != 0) { 
            result.push_back(d);
        }
        test.inference_time.clear();
        test.op_name.clear();
        test.kernel_name.clear();
    }
    
    return result;
}
