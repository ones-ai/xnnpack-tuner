#include <optimizer/optimizer.h>



void optimizer::tuner_hard_reset(std::string ip, int port) { 
    this->ip = ip;
    this->port = port;

    network nwtp(this->ip, this->port);
    nwtp.request("/refresh", "");
    nwtp.wait();
}

std::vector<int> optimizer::tuner_create(const std::vector<int>& workload, int sleep_time) { 
    tuner_size = workload.size();

    rapidjson::Document document;
    document.SetObject();
    rapidjson::Document::AllocatorType &allocator = document.GetAllocator();
    document.AddMember("ports", 9001, allocator);
    document.AddMember("layers", workload.size(), allocator);
    document.AddMember("opt", "tpe", allocator);
    
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    document.Accept(writer);

    network nwtp(this->ip, this->port);
    nwtp.request("/optimizer_setup", strbuf.GetString());
    nwtp.wait();

    std::cout << "sleep start" << std::endl;
    sleep(sleep_time);
    std::cout << "sleep finish" << std::endl;

    return tuner_setup(workload);
}


std::vector<int> optimizer::tuner_setup(const std::vector<int>& workload) { 
    rapidjson::Document document;
    document.SetObject();
    rapidjson::Document::AllocatorType &allocator = document.GetAllocator();
    document.AddMember("ports", 9001, allocator);
    document.AddMember("evals", 1500, allocator);


    rapidjson::Value p(rapidjson::kArrayType);
    for (const auto& w : workload) { 
        p.PushBack(w, allocator);
    }

    document.AddMember("search", p, allocator);
    
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    document.Accept(writer);

    network nwtp(this->ip, this->port);
    nwtp.request("/setup", strbuf.GetString());
    nwtp.wait();

    return tuner_get_workload();
}

void optimizer::p_tuner_next_workload(const std::vector<long long> score) {
    rapidjson::Document document;
    document.SetObject();
    rapidjson::Document::AllocatorType &allocator = document.GetAllocator();
    document.AddMember("ports", 9001, allocator);
    rapidjson::Value p(rapidjson::kArrayType);
    for (const auto& w : score) { 
        p.PushBack((int64_t)w, allocator);
    }
    document.AddMember("scores", p, allocator);

    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    document.Accept(writer);

    network nwtp(this->ip, this->port);
    nwtp.request("/next", strbuf.GetString());
    nwtp.wait();
}
std::vector<int> optimizer::tuner_get_workload() {
    rapidjson::Document document;
    document.SetObject();
    rapidjson::Document::AllocatorType &allocator = document.GetAllocator();
    document.AddMember("ports", 9001, allocator);
    document.AddMember("layers", tuner_size, allocator);
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    document.Accept(writer);

    network nwtp(this->ip, this->port);
    nwtp.request("/get", strbuf.GetString());
    nwtp.wait();

    document.Parse(nwtp.recv_body().c_str());
    if(document.HasParseError()) { 
        throw "error!!";
    }
    
    std::vector<int> result;
    for (auto& v : document.GetArray()) { 
        int i = std::stoi(v.GetString());
        result.push_back(i);
    }

    return result;
}


std::vector<int> optimizer::tuner_next_workload(const std::vector<long long> score) { 
    p_tuner_next_workload(score);
    return tuner_get_workload();
}
