#include <array>
#include <stdio.h>
#include <optimizer/helper.h>
#include <optimizer/optimizer.h>
#include <fstream>
#include <pthreadpool.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <chrono>
#include <sstream>


#define CORE_SPECIFIC 2,4
#define MINIMAL_CPU_COUNT 6

std::vector<int> split(std::string str, char Delimiter) {
    std::istringstream iss(str);             // istringstream에 str을 담는다.
    std::string buffer;                      // 구분자를 기준으로 절삭된 문자열이 담겨지는 버퍼
 
    std::vector<int> result;
 
    int i = 0;
    // istringstream은 istream을 상속받으므로 getline을 사용할 수 있다.
    while (getline(iss, buffer, Delimiter)) {
        i += 1;
        if (i == 1) { 
          continue;
        }
        result.push_back(std::stoi(buffer));               // 절삭된 문자열을 vector에 저장
    }
 
    return result;
}
 
std::vector<std::vector<int>> convert(const std::vector<int>& workload, const std::vector<int>& max_workload) { 
  std::vector<std::vector<int>> result;
  
  std::array<int, 2> core { CORE_SPECIFIC };

  for (int w = 0; w < max_workload.size(); w++) {
    std::vector<int> r;
    std::array<int, 2> cluster_workload { 
      max_workload[w] - workload[w] - 1,
      workload[w]
    };
    for (int c = 0; c < cluster_workload.size(); c++) { 
      int div = cluster_workload[c] / core[c];
      int rem = cluster_workload[c] - (div * core[c]);
      for (int p = 0; p < core[c]; p++) { 
        // printf("%d, ", div + (p < rem));
        r.push_back(div + (p < rem));
      }
      // printf("\n");
    }
    result.push_back(r);
  } 
  return result;
}
std::vector<long long> convert_score(const std::vector<inference_data>& data) { 
  std::vector<long long> result(data[1].inference_time.size());
  long long total = 0;
  for (int i = 0; i < data.size(); i++) { 
    for (int r = 0; r < data[i].inference_time.size(); r++) { 
      result[r] += data[i].inference_time[r] / data.size();
    }
    total += data[i].inference_total_time / data.size();
  }  
  std::cout << "" << total / 1000 << "us \n";
  
  return result;
}

int main(int argc, char* argv[]) {
  printf("CPU CORE SET : %d, %d\n", CORE_SPECIFIC);
  if (argc < 5) {
    fprintf(stderr, "minimal <tflite model> <csv save directory> <gemm_id> <latest_evals> <gemm_kernel>\n");
    return 1;
  }
  std::vector<int> init_gemm;
  std::vector<int> work_size;
  optimizer opt; 

  const char* filename = argv[1];
  std::string dir = argv[2];
  int gemm_id = atoi(argv[3]);
  int latest_evals = atoi(argv[4]);
  if (argc == 6) {  // Kernel Opt Test
    printf("Kernel Opt Test Mode\n");
    std::string gemm_kernel = argv[5];
    std::ifstream gemm_staratagy(dir + "/" + gemm_kernel);
    std::string line;
    std::getline(gemm_staratagy, line);
    auto s = split(line, ',');
    printf("Load : \n");
    for (int i = 0; i < s.size(); i++) { 
      init_gemm.push_back(s[i]);
      printf("%d, ", s[i]);
    }
    printf("\n");

    if (gemm_id == 0) {  // 0 => Test Kernel Opt, other value is optimizing cpu schedule.
      printf("OK. Start Kernel Opt Test\n");
      std::ofstream gemm_result(dir + "/gemm_opts.txt");

      pthread_default_mode = 1;
      auto init = opt.init(MINIMAL_CPU_COUNT, filename, init_gemm);

      std::vector<int> gemm_idx;
      for (int i = 0; i < init.name.size(); i++) { 
        if (init.name[i].kernel.find("GEMM") != std::string::npos){ 
          gemm_idx.push_back(i);
        } if (init.name[i].kernel.find("DWConv") != std::string::npos){ 
          gemm_idx.push_back(i);
        }
        // 
      }

      std::cout << "GEMM COUNT : " << init.gemm_config_cnt << ", " << init.gemm_cnt << ", " << gemm_idx.size() << "\n";

      pthread_default_mode = 1;
      auto observe = opt.inference(std::vector<std::vector<int>>(),
                                  latest_evals + 1, false);

      for (int i = 0; i < observe.size(); i++) { 
        for (int k = 0; k < gemm_idx.size(); k++) { 
          gemm_result << observe[i].inference_time[k] << ",";
        }
        std::cout << observe[i].inference_total_time / 1000 << "us" << std::endl;
        gemm_result << "\n";
      }
      return 0;
    }
  }

  std::vector<std::ofstream> csvs;
  
  printf("You can start cpu opt mode\n");
  if (gemm_id >= 0) { 
    printf("do you want gemm opt?\n");
    for (int i = 0; i < 10000; i++) { 
      init_gemm.push_back(gemm_id);
    }
  }

  if (gemm_id == -3) { 
    printf("Default evaluate!~\n");
    pthread_default_mode = 1;
    auto init = opt.init(MINIMAL_CPU_COUNT, filename, init_gemm);
    return 0;
  }
  
  pthread_default_mode = 1;
  auto init = opt.init(MINIMAL_CPU_COUNT, filename, init_gemm);
  pthread_default_mode = 0;

  std::cout << "GEMM COUNT : " << init.gemm_config_cnt << ", " << init.gemm_cnt << "\n";
  
  std::cout << "Op Name\t\t Kernel Name\t\t WorkSize\n";
  for (int i = 0; i < init.name.size(); i++) {
    // const auto& w : init.name
    auto w = init.name[i];
    
    if (gemm_id != -2){
      csvs.push_back(std::ofstream(dir + "/" + std::to_string(i + 1) + "_" +
                                  w.kernel + "_" + w.ops + "_" +
                                  std::to_string(w.workload)));
      csvs[i] << "gemm_id,max_work_size";
      for (int a = 0; a < MINIMAL_CPU_COUNT; a++) {
        csvs[i] << "cpu" << a << ",";
      }
      for (int s = 0; s < 8; s++) {
        csvs[i] << "score" << s << ",";
      }
      csvs[i] << "\n";
    }
    work_size.push_back(w.workload + 1);
    std::cout << w.ops << "\t\t" << w.kernel << "\t\t" << w.workload + 1
              << "\n";
  }

  if (gemm_id == -1){ // Cpu Opt
    printf("starting cpu opt\n");

    int max_infer_count = 0;
    for (const auto& w : work_size) {
      max_infer_count = std::max(max_infer_count, w);
    }
    std::cout << max_infer_count << "  asdfasdf\n\n";

    // opt.tuner_hard_reset("192.168.50.134", 9000);
    // opt.tuner_hard_reset("220.68.5.97", 9000);
    // auto seach = opt.tuner_create(work_size, 25);

    std::vector<int> get_best_work_size(work_size.size());
    std::vector<long long> get_score_work_size(work_size.size());

    // std::vector<int> aasdasd {530, 1241, 394, 21, 46, 1734, 59, 98, 11, 35,
    // 2393, 45, 1146, 72, 4660, 90, 769, 48, 3472, 60, 763, 6, 16, 15, 124, 123,
    // 32};

    for (int i = 0; i < work_size.size(); i++) {
      get_best_work_size[i] = 0;
      get_score_work_size[i] = INT32_MAX;
    }

    auto seach = std::vector<int>(work_size.size());

    for (int i = 0; i < max_infer_count; i++) {
      // auto starttt = std::chrono::high_resolution_clock::now();

      for (int j = 0; j < work_size.size(); j++) {
        seach[j] = i % work_size[j];
      }
      // std::cout << "1 : " << (std::chrono::high_resolution_clock::now() -
      // starttt).count() / 1000 / 1000 << "ms\n\n";

      auto s = convert(seach, work_size);
      for (int j = 0; j < work_size.size(); j++) {
        csvs[j] << gemm_id << "," << work_size[j] << ",";
        for (int a = 0; a < s[j].size(); a++) {
          csvs[j] << s[j][a] << ",";
        }
      }

      // std::cout << "2 : " << (std::chrono::high_resolution_clock::now() -
      // starttt).count() / 1000 / 1000 << "ms\n\n";

      auto observe = opt.inference(convert(seach, work_size), 9, true);

      // std::cout << "3 : " << (std::chrono::high_resolution_clock::now() -
      // starttt).count() / 1000 / 1000 << "ms\n\n";

      for (int k = 0; k < work_size.size(); k++) {  // kernel size
        for (int j = 0; j < observe.size(); j++) {  // 8
          csvs[k] << observe[j].inference_time[k] << ",";
        }
        csvs[k] << "\n";
        csvs[k].flush();
      }

      double xnn_avg = 0;
      double total_avg = 0;

      for (int k = 0; k < observe.size(); k++) {
        xnn_avg += observe[k].xnnpack_inference_total_time / 8;
        total_avg += observe[k].inference_total_time / 8;
      }

      // std::cout << "4 : " << (std::chrono::high_resolution_clock::now() -
      // starttt).count() / 1000 / 1000 << "ms\n\n"; csvs[j] << "\n";

      auto score = convert_score(observe);
      for (int j = 0; j < work_size.size(); j++) {
        if (get_score_work_size[j] > score[j]) {
          get_score_work_size[j] = score[j];
          get_best_work_size[j] = seach[j];
        }
        // printf("%d, ", get_best_work_size[j]);
      }

      // std::cout << "5 : " << (std::chrono::high_resolution_clock::now() -
      // starttt).count() / 1000 / 1000 << "ms\n\n";
      std::cout << "(" << i << "/" << max_infer_count << ")|||  xnnpack avg "
                << xnn_avg / 1000 / 1000 << " ms ||| total avg "
                << total_avg / 1000 / 1000 << " ms\n";
      // seach = opt.tuner_next_workload(score);
      // std::cout << "|||  avg " << avg << " us, bes1t :  " << best << " us\n";
    }

    // printf("\n");
    std::ofstream best_models(dir + "/get_best.txt");

    pthread_default_mode = 1;
    auto observe = opt.inference(convert(get_best_work_size, work_size),
                                latest_evals + 1, false);

    best_models << "xnnpack_total_time,";
    for (int j = 0; j < observe.size(); j++) {
      best_models << observe[j].xnnpack_inference_total_time << ",";
    }
    best_models << "\ntflite_total_time,";
    for (int j = 0; j < observe.size(); j++) {
      best_models << observe[j].inference_total_time << ",";
    }
    best_models << "\n";

    best_models << "opts,";
    for (int j = 0; j < work_size.size(); j++) {
      best_models << get_best_work_size[j] << ",";
    }
    best_models << "\n";
    for (int j = 0; j < observe.size(); j++) {      // 8
      for (int k = 0; k < work_size.size(); k++) {  // kernel size
        best_models << observe[j].inference_time[k] << ",";
      }
      best_models << "\n";
    }

  }
  else if (gemm_id == -2) {  // Repeat CpuOpt
    printf("repeat cpu opt result\n");
    std::ifstream best_models(dir + "/get_best.txt");
    std::string line;
    getline(best_models, line);
    std::cout << line << std::endl;
    getline(best_models, line);
    std::cout << line << std::endl;
    getline(best_models, line);
    std::cout << line << std::endl;

    auto s = split(line, ',');
    s.pop_back();

    for (int i = 0; i < s.size(); i++) { 
      std::cout << s[i] << ",";
    }
    std::cout << std::endl;
    

    pthread_default_mode = 0;
    auto observe = opt.inference(convert(s, work_size),
                                latest_evals + 1, false);
    for (int i = 0; i < observe.size(); i++) { 
      std::cout << observe[i].inference_total_time << "\n";
    }
  }
  else { // Kernel Opt
    printf("gemm kernel opt test\n");
    std::ofstream gemm_result(dir + "/gemm_" + std::to_string(gemm_id) + ".txt");
    std::ofstream gemm_result_time(dir + "/gemm_time" + std::to_string(gemm_id) + ".txt");

    pthread_default_mode = 1;
    std::vector<int> gemm_idx;
    for (int i = 0; i < init.name.size(); i++) { 
      if (init.name[i].kernel.find("GEMM") != std::string::npos){ 
        gemm_idx.push_back(i);
      } if (init.name[i].kernel.find("DWConv") != std::string::npos){ 
          gemm_idx.push_back(i);
      }
    }

    pthread_default_mode = 1;
    auto observe = opt.inference(std::vector<std::vector<int>>(),
                                latest_evals + 1, true);

    for (int i = 0; i < observe.size(); i++) { 
      for (int k = 0; k < gemm_idx.size(); k++) { 
        gemm_result << observe[i].inference_time[k] << ",";
      }
      gemm_result_time << observe[i].inference_total_time << "\n";
      gemm_result << "\n";
      std::cout << observe[i].inference_total_time / 1000 << "us\n";
    }
    return 0;
  }
  return 0;
}
