#!/bin/bash

model_path=$1
mode=$2
model_name=$(basename "$model_path")
model_name="${model_name%.*}"

test -d "inference/${model_name}" || mkdir -p "inference/${model_name}"

if [ -f "inference/${model_name}/${mode}.txt" ]; then
    rm "inference/${model_name}/${mode}.txt"
fi

sudo sync
echo 3 | sudo tee /proc/sys/vm/drop_caches
LD_LIBRARY_PATH=. ./acltuner-${mode}.sh ${model_path} >> inference/${model_name}/${mode}.txt

python3 1_clean_log.py inference/${model_name}/${mode}.txt
python3 2_extract_infer_time.py inference/${model_name}/${mode}.txt
python3 3_truncated_mean.py inference/${model_name}/${mode}.txt


