import sys
import numpy as np

def calculate_median(filename):
    with open(filename, 'r') as file:
        values = np.array([float(line.strip()) for line in file])

    # 중앙값 계산
    median = np.median(values)
    return median

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python median_calculator.py <filename>")
    else:
        filename = sys.argv[1]
        median = calculate_median(filename)
        print(f"The median of the values is: {median}")
