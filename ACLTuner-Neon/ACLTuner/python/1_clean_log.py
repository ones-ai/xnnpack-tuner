import os
import tempfile
import sys

def clean_inference_log(file_path):
    # 임시 파일 생성
    temp_file = tempfile.NamedTemporaryFile(mode='w', delete=False)

    with open(file_path, 'r') as infile, temp_file as outfile:
        # "inference time :"가 시작되기 전까지 모든 내용을 건너뜁니다.
        for line in infile:
            if line.startswith('inference time :'):
                outfile.write(line)
                break

        # 나머지 파일 내용을 복사합니다.
        for line in infile:
            outfile.write(line)

    # 임시 파일을 원본 파일로 대체
    os.replace(temp_file.name, file_path)

# 명령줄 인터페이스
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python clean_log.py <filename>")
    else:
        file_path = sys.argv[1]
        clean_inference_log(file_path)
