import re
import sys
import os
import tempfile

def extract_and_save_inference_times(input_filename):
    # 임시 파일 생성
    temp_file = tempfile.NamedTemporaryFile(mode='w', delete=False)

    with open(input_filename, 'r') as infile, temp_file as outfile:
        for line in infile:
            match = re.search('inference time : ([\d.]+)', line)
            if match:
                outfile.write(match.group(1) + '\n')

    # 임시 파일을 원본 파일로 대체
    os.replace(temp_file.name, input_filename)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python extract_inference.py <input_file>")
    else:
        input_file = sys.argv[1]
        extract_and_save_inference_times(input_file)
