import sys

def calculate_middle_80_percent_average(filename):
    with open(filename, 'r') as file:
        values = [float(line.strip()) for line in file]

    values.sort()  # 값 정렬

    # 상위 10% 및 하위 10%를 제외하고 중위 80%의 데이터 계산
    ten_percent = len(values) // 10
    middle_values = values[ten_percent:-ten_percent]

    # 중위 80%의 평균 계산
    middle_average = sum(middle_values) / len(middle_values)
    return middle_average

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python middle_80_average.py <filename>")
    else:
        filename = sys.argv[1]
        average = calculate_middle_80_percent_average(filename)
        print(f"The average of the middle 80% is: {average}")
