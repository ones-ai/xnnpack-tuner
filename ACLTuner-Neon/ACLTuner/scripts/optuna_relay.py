from flask import request, Flask, Response
import argparse
import json
import os
from multiprocessing import Process
import requests

app = Flask(__name__)

@app.route("/refresh", methods=['POST'])
def flask_refresh():
    return os.system("pkill -f 'optuna_optimizer'")
    

@app.route('/next', methods=['POST'])
def flask_next():
    data = request.get_json()
    result = []
    
    for idx in range(len(data['scores'])):
        port = data['ports'] + idx
        score = data['scores'][idx]
        res = requests.get("http://127.0.0.1:" + str(port) + "/next?score=" + str(score))
        result.append(res.content.decode("utf-8"))
        
    return Response(json.dumps(result), mimetype='application/json', status=200)

@app.route('/get', methods=['POST'])
def flask_get():
    data = request.get_json()
    result = []
    
    for item in range(data['layers']):
        port = data['ports'] + item
        res = requests.get("http://127.0.0.1:" + str(port) + "/get")
        result.append(res.content.decode("utf-8"))
    return Response(json.dumps(result), mimetype='application/json', status=200)

@app.route('/setup', methods=['POST'])
def flask_setup():
    data = request.get_json()
    result = []

    for item in range(len(data['search'])):
        port = item + data['ports']
        seach_space = data['search'][item]
        evals = data['evals']
        res = requests.get("http://127.0.0.1:" + str(port) + "/setup?evals=" 
                            + str(evals)
                            + "&total=" + str(seach_space))
        result.append(res.content.decode("utf-8"))

    return Response(json.dumps(result), mimetype='application/json', status=200)

def run_optimizer(ports, opt):
    os.system("python3 ./optuna_optimizer.py -p " + str(ports) + " -o " + opt)

@app.route('/optimizer_setup', methods=['POST'])
def flask_opt_setup():
    data = request.get_json()

    print(data)
    for idx in range(data['layers']):
        port = idx + data['ports']
        opt_mode = data['opt']
        p1 = Process(target=run_optimizer, args=(port, opt_mode), daemon=True)
        p1.start()

    return Response("success", mimetype='application/json', status=200)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=9000, debug=False)
