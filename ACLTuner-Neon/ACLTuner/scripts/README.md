# API

## <B> http://[ip]:[port]/optimizer_setup
```
BODY, METHOD : POST
{
	"ports": 9001,
    "layers": 3,
    "opt": "random" 
    # random, grid, tpe, cma, pf, nsga2, qmc
}
```

## <B> http://[ip]:[port]/setup
```
BODY, METHOD : POST
{
	"ports": 9001,
	"evals": 100,
	"search": [300, 400, 500]
}
```

## <B> http://[ip]:[port]/get
```
BODY, METHOD : POST
{
	"ports": 9001,
    "layers": 3
}
```

## <B> http://[ip]:[port]/next
```
BODY, METHOD : POST
{
	"ports": 9001,
    "scores": [100, 200, 300]
}
```
