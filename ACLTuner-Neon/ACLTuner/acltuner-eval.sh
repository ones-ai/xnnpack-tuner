#!/bin/bash

model_path=$1
model_name=$(basename "$model_path")
model_name="${model_name%.*}"

server_ip=${2:-"192.168.0.1"}
server_port=${3:-"22"}

repeat=1
evals=1500

./chacha --mode log \
         --model ${model_path} \
         --repeat ${repeat} \
         --evals ${evals} \
         --tune ./tune/${model_name}-for-eval.json \
         --log ./log/${model_name}-for-eval-log.json \
         --host ${server_ip} \
         --port ${server_port}
