#ifndef MAIN_NEON_INTERFACE_INFERENCE_ENGINE_H
#define MAIN_NEON_INTERFACE_INFERENCE_ENGINE_H

#include <luna/armnn.h>
#include <memory>
#include <stb/stb_image.h>

template<typename Tout>
class neon_engine { 
private:
    std::shared_ptr<common::ArmnnNetworkExecutor<Tout>> target;
    unsigned char* data;

public:
    std::string get_name() const { 
        return "neon";
    }

    void init(const std::string file) { 
        std::string path = ("./" + file); 
        std::vector<armnn::BackendId> backend {"CpuAcc"};
        printf("neon!!\n\n\n");
        this->target = std::make_shared<common::ArmnnNetworkExecutor<Tout>>(path,
                                                                            backend,
                                                                            false);
        data = (unsigned char*)malloc(500 * 500 * 4 * sizeof(Tout));
    }
    void inference()  {
        common::InferenceResults<Tout> results;
        int width = 224, height = 224, c = 3;
        this->target->Run((unsigned char*)data, width * height * c * 1, results);
    }

    void deinit() { 
        free(data);
    }

};

#endif // MAIN_NEON_INTERFACE_INFERENCE_ENGINE_H
