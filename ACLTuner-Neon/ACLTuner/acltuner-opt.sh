#!/bin/bash

kernel_ops=5

model_path=$1
model_name=$(basename "$model_path")
model_name="${model_name%.*}"

server_ip=${2:-"192.168.0.1"}
server_port=${3:-"22"}

repeat=8
evals=1500

test -d "tune/${model_name}" || mkdir -p tune/${model_name}
test -d "log/${model_name}"  || mkdir -p log/${model_name}

for ((conv=-1; conv < $kernel_ops; conv++)); do
    ./chacha --mode optimize \
             --model ${model_path} \
             --repeat ${repeat} \
             --evals ${evals} \
             --tune tune/${model_name}/${conv}.json \
             --conv ${conv} \
             --log log/${model_name}/"log_(${conv})".json \
             --host ${server_ip} \
             --port ${server_port}
done

python3 integrate_convolution.py -t tune/${model_name} \
                                 -o tune/${model_name}-for-eval.json
