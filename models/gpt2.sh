#!/bin/bash

# 파라미터 설정
versions=(small)
batchs=(1)
sequences=(64 128 256 384 512)

# 디렉토리 생성
mkdir -p gpt2

# 디렉토리 이동
cd gpt2

# 모델 생성
for version in "${versions[@]}"
do
    for batch in "${batchs[@]}"
    do
        for sequence in "${sequences[@]}"
        do
            echo
            python3 ../export_gpt2_tflite.py --version $version --batch $batch --sequence $sequence
        done
    done
done

# 디렉토리 이동
cd ../
