import tensorflow as tf
from transformers import TFBertModel, BertConfig
import argparse

# 명령줄 인수 설정
parser = argparse.ArgumentParser(description='Setup BERT model parameters and convert to TFLite.')
parser.add_argument('--version', type=str, choices=['base', 'large'], default='base', required=False, help='Model size: base or large')
parser.add_argument('--casing', type=str, choices=['uncased', 'cased'], default='uncased', required=False, help='Text casing: uncased or cased')
parser.add_argument('--batch', type=int, default=1, help='Batch size')
parser.add_argument('--sequence', type=int, default=384, help='Sequence length: 1-512')

args = parser.parse_args()

version = args.version
casing = args.casing
batch = args.batch
sequence = args.sequence

# BERT 모델 설정
if version in ['base', 'large'] and casing in ['uncased', 'cased']:
    model_name =f'google-bert/bert-{version}-{casing}'
else:
    raise ValueError("invalid mode")

config = BertConfig.from_pretrained(model_name)
bert_model = TFBertModel.from_pretrained(model_name, config=config)

# TFLite 변환을 위한 서빙 함수 정의
@tf.function(input_signature=[
    tf.TensorSpec([batch, sequence], tf.int32, name="input_ids"),
    tf.TensorSpec([batch, sequence], tf.int32, name="attention_mask"),
    tf.TensorSpec([batch, sequence], tf.int32, name="token_type_ids")
])
def serve(input_ids, attention_mask, token_type_ids):
    return bert_model(input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids)

# TFLite 모델로 변환
converter = tf.lite.TFLiteConverter.from_concrete_functions([serve.get_concrete_function()])
tflite_model = converter.convert()

# TFLite 모델 저장
tflite_name =f'bert_{version}_{batch}_{sequence}.tflite'
with open(tflite_name, 'wb') as f:
    f.write(tflite_model)