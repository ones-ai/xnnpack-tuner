import tensorflow as tf
from transformers import TFGPT2Model, GPT2Config
import argparse
import os


# 명령줄 인수 설정
parser = argparse.ArgumentParser(description='Setup GPT2 model parameters and convert to TFLite.')
parser.add_argument('--version', type=str, choices=['small', 'medium', 'large', 'xl'], default='small', required=False, help='Model size: small, medium, large, xl')
parser.add_argument('--batch', type=int, default=1, help='Batch size')
parser.add_argument('--sequence', type=int, default=256, help='Sequence length')

args = parser.parse_args()

version = args.version
batch = args.batch
sequence = args.sequence

# GPT-2 모델 설정
if version == 'small':
    model_name = 'openai-community/gpt2'
elif version in ['medium', 'large', 'xl']:
    model_name = f'openai-community/gpt2-{version}'
else:
    raise ValueError("invalid mode")
print(model_name)

config = GPT2Config.from_pretrained(model_name)
gpt2_model = TFGPT2Model.from_pretrained(model_name, config=config)

# TFLite 변환을 위한 서빙 함수 정의
@tf.function(input_signature=[
    tf.TensorSpec([batch, sequence], tf.int32, name="input_ids"),
    tf.TensorSpec([batch, sequence], tf.int32, name="attention_mask")
])
def serve(input_ids, attention_mask):
    return gpt2_model(input_ids, attention_mask=attention_mask)

# TFLite 모델로 변환
converter = tf.lite.TFLiteConverter.from_concrete_functions([serve.get_concrete_function()])
tflite_model = converter.convert()


# TFLite 모델 저장
tflite_name =f'gpt2_{version}_{batch}_{sequence}.tflite'
with open(tflite_name, 'wb') as f:
    f.write(tflite_model)
