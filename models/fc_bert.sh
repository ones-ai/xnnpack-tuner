#!/bin/bash

# 디렉토리 생성
mkdir -p fc_bert

# 디렉토리 이동
cd fc_bert

# Parameter
sequences=(64 128 256 384 512)
hidden_dim=768         # base:768  large:1024
intermediate_dim=3072  # base:3072 large:4096
head_dim=64            # head_dim = hidden_dim / attention_head

# Output Layer
size0="$hidden_dim $hidden_dim $hidden_dim"
# IFS (Internal Field Separator)를 사용하여 문자열을 변수로 읽기
IFS=' ' read -r input hidden output <<< "$size0"
python3 ../export_fc_tflite.py --input $input --hidden $hidden --output $output

# Loop
for sequence in "${sequences[@]}"
do
    size1="$sequence $hidden_dim $hidden_dim"
    size2="$sequence $hidden_dim $intermediate_dim"
    size3="$sequence $intermediate_dim $hidden_dim"
    size4="$sequence $sequence $head_dim"
    size5="$sequence $head_dim $sequence"

    # 문자열들을 포함하는 배열 선언
    sizes=("$size1" "$size2" "$size3" "$size4" "$size5")

    for size in "${sizes[@]}"
    do
        IFS=' ' read -r input hidden output <<< "$size"
        python3 ../export_fc_tflite.py --input $input --hidden $hidden --output $output
    done
done

# 디렉토리 이동
cd ../