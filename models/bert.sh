#!/bin/bash

# 파라미터 설정
versions=(base)
casings=(uncased)
batchs=(1)
sequences=(64 128 256 384 512)

# 디렉토리 생성
mkdir -p bert

# 디렉토리 이동
cd bert

# 모델 생성
for version in "${versions[@]}"
do
    for casing in "${casings[@]}"
    do
        for batch in "${batchs[@]}"
        do
            for sequence in "${sequences[@]}"
            do
                python3 ../export_bert_tflite.py --version $version --casing $casing --batch $batch --sequence $sequence
            done
        done
    done
done

# 디렉토리 이동
cd ../
