#!/bin/bash

# 배열 선언
Ns=(32 64 96 128 192 256 320 384 440 512 640 768 896 1024 1280 1536 1792 1920)

# 디렉토리 생성
mkdir -p fc_nnn

# 디렉토리 이동
cd fc_nnn

# 배열 요소를 반복 처리
for N in "${Ns[@]}"
do
    python3 ../export_fc_tflite.py --input $N --hidden $N --output $N
done

# 디렉토리 이동
cd ../