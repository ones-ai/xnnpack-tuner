import tensorflow as tf
from transformers import TFGPT2LMHeadModel, GPT2Config

# 파라미터 설정
mode = 'base'
batch = 1
sequence = 64

# GPT-2 모델 설정
model_name = f'gpt2'
config = GPT2Config.from_pretrained(model_name)
gpt2_model = TFGPT2LMHeadModel.from_pretrained(model_name, config=config)

# TFLite 변환을 위한 서빙 함수 정의
@tf.function(input_signature=[
    tf.TensorSpec([batch, sequence], tf.int32, name="input_ids"),
    tf.TensorSpec([batch, sequence], tf.int32, name="attention_mask")
])
def serve(input_ids, attention_mask):
    return gpt2_model(input_ids, attention_mask=attention_mask)

# TFLite 모델로 변환
converter = tf.lite.TFLiteConverter.from_concrete_functions([serve.get_concrete_function()])
tflite_model = converter.convert()

# TFLite 모델 저장
with open(f'gpt2_{mode}_{sequence}.tflite', 'wb') as f:
    f.write(tflite_model)
