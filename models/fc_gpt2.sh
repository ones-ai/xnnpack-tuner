#!/bin/bash

# 디렉토리 생성
mkdir -p fc_gpt2

# 디렉토리 이동
cd fc_gpt2

# Parameter
sequences=(64 128 256 384 512)
hidden_dim=768
intermediate_dim=3072
intermediate_hidden_dim=2304
head_dim=64

# Loop
for sequence in "${sequences[@]}"
do
    size1="$sequence $hidden_dim $intermediate_hidden_dim"
    size2="$sequence $hidden_dim $hidden_dim"
    size3="$sequence $hidden_dim $intermediate_dim"
    size4="$sequence $intermediate_dim $hidden_dim"
    size5="$sequence $sequence $head_dim"
    size6="$sequence $head_dim $sequence"

    # 문자열들을 포함하는 배열 선언
    sizes=("$size1" "$size2" "$size3" "$size4" "$size5" "$size6")

    for size in "${sizes[@]}"
    do
        IFS=' ' read -r input hidden output <<< "$size"
        python3 ../export_fc_tflite.py --input $input --hidden $hidden --output $output
    done
done

# 디렉토리 이동
cd ../