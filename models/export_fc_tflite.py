import tensorflow as tf
from tensorflow.keras.layers import Input, Dense
import argparse
import os

# 명령줄 인수 설정
parser = argparse.ArgumentParser(description='Setup FC model parameters and convert to TFLite.')
parser.add_argument('--input', type=int, default=512, help='Input size')
parser.add_argument('--hidden', type=int, default=512, help='Hidden size')
parser.add_argument('--output', type=int, default=512, help='Output size')

args = parser.parse_args()

input_size = args.input
hidden_size = args.hidden
output_size = args.output

def fullyconnected(input_size, hidden_size, output_size):
    model = tf.keras.Sequential()
    model.add(Input(batch_shape=(input_size, hidden_size)))
    model.add(Dense(output_size, activation=None, use_bias=True, bias_initializer=tf.keras.initializers.GlorotUniform()))
    return model

model = fullyconnected(input_size, hidden_size, output_size)

# Convert the model to TFLite format
converter = tf.lite.TFLiteConverter.from_keras_model(model)
tflite_model = converter.convert()

# Save the TFLite model
tflite_name = f'fc_{input_size}_{hidden_size}_{output_size}.tflite'
with open(tflite_name, 'wb') as f:
    f.write(tflite_model)